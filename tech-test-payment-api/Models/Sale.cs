﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int Id { get; set; }
        [Required]
        public Seller Seller { get; set; }
        public DateTime Date { get; set; }

        // Must have at least one
        [NotMapped]
        public IEnumerable<string> Items { get; set; }
        public string Status { get; set; }
    }
}