﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
        public static List<Sale> Sales = new List<Sale>();
        private static int lastIndex;

        private List<string> PossibleStatus = new List<string>(){
            "Aguardando Pagamento",
            "Pagamento Aprovado",
            "Cancelada",
            "Enviado para Transportadora",
            "Entregue"
        };

        [HttpPost]
        public IActionResult RegisterSale(Sale sale)
        {
            if (!sale.Items.Any())
                return BadRequest(new { Erro = "A lista de itens nao pode ser vazia" });

            if (sale.Seller == null)
                return BadRequest(new { Erro = "A venda deve ser cadastrada com algum vendedor" });

            sale.Status = "Aguardando Pagamento";
            sale.Id = lastIndex;
            lastIndex++; // TODO. Change to get database id later.
            Sales.Add(sale);

            return Ok(sale);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            foreach (Sale sale in Sales)
            {
                if (sale.Id == id)
                    return Ok(sale);
            }

            return NotFound();
        }

        [HttpPut("Changestatus/{id}")]
        public IActionResult ChangeStatus(int id, string status)
        {
            Sale saleToChange = null;
            foreach (Sale sale in Sales)
            {
                if (sale.Id == id)
                    saleToChange = sale;
                    
            }

            if (saleToChange == null)
                return NotFound();

            if (CanChangeStatus(saleToChange.Status, status))
                saleToChange.Status = status;
            else
                return BadRequest(new { Erro = "Não é possível alterar para esse status." });
            

            return Ok(saleToChange);
        }

        /*  
            "Aguardando Pagamento",
            "Pagamento Aprovado",
            "Cancelado",
            "Enviado Para Transportadora",
            "Entregue"
        */

        // De: Aguardando pagamento(0)  Para: Pagamento Aprovado(1)
        // De: Aguardando pagamento(0)   Para: Cancelada(2)
        // De: Pagamento Aprovado(1)    Para: Enviado para Transportadora(3)
        // De: Pagamento Aprovado(1)   Para: Cancelada(2)
        // De: Enviado para Transportador(3)  Para: Entregue(4)

        private bool CanChangeStatus(string currentStatus, string newStatus)
        {
            if (!PossibleStatus.Contains(newStatus)) return false;
            
            if (currentStatus == PossibleStatus[0])
            {
                return (newStatus == PossibleStatus[1] || newStatus == PossibleStatus[2]);
            }
            else if (currentStatus == PossibleStatus[1])
            {
                return (newStatus == PossibleStatus[2] || newStatus == PossibleStatus[3]);
            }
            else if (currentStatus == PossibleStatus[3])
            {
                return newStatus == PossibleStatus[4];
            }
            
            return false;
        }
    }
}