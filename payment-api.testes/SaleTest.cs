﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Controllers;
using tech_test_payment_api.Models;
using Xunit.Abstractions;

namespace payment_api.testes
{
    public class SaleTest
    {
        private SalesController salesController;
        public ITestOutputHelper consoleTestOutput;

        private Seller seller;
        private Sale sale;

        public SaleTest(ITestOutputHelper consoleTestOutput)
        {
            this.consoleTestOutput = consoleTestOutput;
            salesController = new SalesController();

            seller = new Seller()
            {
                Id = 0,
                CPF = "111.111.111-11",
                Name = "Test",
                Email = "Teste@email.com",
                Telefone = "2222-2222"
            };

            sale = new Sale()
            {
                Id = 0,
                Seller = seller,
                Date = DateTime.MinValue,
                Items = new List<String>() { "test" },
                Status = "Aguardando Pagamento"
            };
        }

        [Fact]
        public void Register_Sale()
        {
            // Arrange
            // Act
            var result = salesController.RegisterSale(sale);

            // Assert
            int expectedId = 0;
            Assert.Contains(SalesController.Sales, item => item.Id == expectedId);
        }

        [Fact]
        public void Get_Sale_By_Id()
        {
            // Arrange
            // Act

            int expectedId = 0;
            var result =  salesController.GetById(expectedId) as OkObjectResult;
            var returnSale = result?.Value as Sale;

            string newStatus = "Pagamento Aprovado";
            sale.Status = newStatus;

            // Assert
            Assert.Equal(sale, returnSale);
        }

        [Fact]
        public void Update_Status_Sale_By_Id()
        {
            // Arrange
            // Act

            string newStatus = "Pagamento Aprovado";
            sale.Status = newStatus;

            var result = salesController.ChangeStatus(0, newStatus) as OkObjectResult;
            var returnSale = result?.Value as Sale;

            // Assert
            Assert.Equal(sale, returnSale);
        }
    }
}